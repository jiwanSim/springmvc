package com.sicc.jiwan;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SiccController {

	@RequestMapping(value = "/")
	public String Hello_World(Model model) throws Exception {
		return "index";
	}
}
